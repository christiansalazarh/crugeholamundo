<h1>Tester</h1>

<div style='padding: 3px; background-color: rgb(255,255,230); width: 400px;'>
<p class='hint'>este boton ejecutara el action siteController.php::actionAjaxCrearUsuario()
una vez presionado puedes revisar los usuarios creados consultando la <br/>
	<?php echo Yii::app()->user->ui->getUserManagementAdminLink("lista de usuarios")?>.
</p>

<b><span style='color: darkred'>Importante:</span> previamente crea dos campos personalizados llamados 
'firstname' y 'lastname', para efectos de demostracion solamente.</b>

<?php
	echo CHtml::ajaxButton("Crear un usuario usando el API de Cruge via Ajax",
		array('/site/ajaxCrearUsuario'),
		array (
			'success'=>"function(data){ $('#result1').html(data); }",
			'error'=>"function(e){ $('#result1').html('error:'+e.responseText); }"
		),
		array()
	);
?>
<div id='result1'></div>

<a href='http://yiiframeworkenespanol.org/wiki/index.php?title=Cruge#EJEMPLOS_DEL_USO_DEL_API_DE_CRUGE:'>mas funciones del api de cruge aqui</a>

</div>
